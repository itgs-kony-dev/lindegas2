﻿
using System;
using System.Reflection;
using System.IO;
using System.Collections.Generic;

using Xamarin;
using Xamarin.Forms;

using Newtonsoft.Json;

namespace LindeGas2
{
	public partial class App : Xamarin.Forms.Application
	{
		public static RootObject RootObject { get; private set; }

		public App()
		{
			InitializeComponent();

			var assembly = typeof(App).GetTypeInfo().Assembly;
			Stream stream = assembly.GetManifestResourceStream("Emissions.json");

			using (var reader = new StreamReader(stream))
			{
				var json = reader.ReadToEnd();
				RootObject = JsonConvert.DeserializeObject<RootObject>(json);
			}

			MainPage = new NavigationPage(new MaterialPage());

		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}

