﻿using System;

namespace LindeGas2
{
	public class RootObject
	{
		public Emission[] Emissions { get; set; }
		public Metal[] Metals { get; set; }
		public Process[] Processes { get; set; }
		public Gas[] Gases { get; set; }
		public CrossSection[] CrossSection { get; set; }
		public WikiData[] WikiData { get; set; }
		public TechnicalData[] TechnicalData { get; set; }
		public Model3DScan[] Model3DScan { get; set; }
	}
}

