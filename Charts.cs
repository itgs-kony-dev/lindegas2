using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Urho;
using Urho.Actions;
using Urho.Gui;
using Urho.Shapes;

namespace FormsSample
{
	public class Charts : Application
	{
		bool movementsEnabled;
		Scene scene;
		Node plotNode;
		Camera camera;
		Octree octree;
		List<Bar> bars;
		Material material;
		Color diffColor = new Color(1.0f, 1.0f, 1.0f);
		Color specColor = new Color(0.0f, 0.05f, 0.2f);
		StaticModel plane;
		Node baseNode;

		public Bar SelectedBar { get; private set; }

		public IEnumerable<Bar> Bars => bars;

		public Charts(/*ApplicationOptions options = null*/) : base(new ApplicationOptions(assetsFolder: "Data")) { }

		//public SamplyGame() : base(new ApplicationOptions(assetsFolder: "Data") { Height = 1024, Width = 576, Orientation = ApplicationOptions.OrientationType.Portrait}) { }

		protected override void Start ()
		{
			base.Start ();
			CreateScene ();
			SetupViewport ();
		}

		async void CreateScene ()
		{
			Input.SubscribeToTouchEnd(OnTouched);

			var cache = ResourceCache;
			scene = new Scene ();
			octree = scene.CreateComponent<Octree> ();

			var cameraNode = scene.CreateChild ("camera");
			camera = cameraNode.CreateComponent<Camera>();
			//cameraNode.Position = new Vector3(0, 5, 0);
			cameraNode.Position = new Vector3(10, 15, 10) / 1.75f;
			cameraNode.Rotation = new Quaternion(-0.121f, 0.878f, -0.305f, -0.35f);

			plotNode = scene.CreateChild();

			/*var planeNode = plotNode.CreateChild("Plane");
			planeNode.Scale = new Vector3(100, 1, 100);
			var planeObject = planeNode.CreateComponent<StaticModel>();
			planeObject.Model = cache.GetModel("Models/Plane.mdl");
			planeObject.SetMaterial(cache.GetMaterial("Materials/StoneTiled.xml"));*/

			/*var rand = new Random();
			for (int i = 0; i < 200; i++)
			{
				var mushroom = plotNode.CreateChild("Mushroom");
				mushroom.Position = new Vector3(rand.Next(90) - 45, 0, rand.Next(90) - 45);
				mushroom.Rotation = new Quaternion(0, rand.Next(360), 0);
				mushroom.SetScale(0.5f + rand.Next(20000) / 10000.0f);
				var mushroomObject = mushroom.CreateComponent<StaticModel>();
				mushroomObject.Model = cache.GetModel("Models/Mushroom.mdl");
				mushroomObject.SetMaterial(cache.GetMaterial("Materials/Mushroom.xml"));
			}*/

			baseNode = plotNode.CreateChild().CreateChild();
			//baseNode.Scale = new Vector3(.01f, .01f, .01f);
			//plotNode.Scale = new Vector3(1f, 1f, 1f);
			//baseNode.Position = new Vector3(3 / 2f - 1, 0, 3 / 2f - 1);
			baseNode.Scale = new Vector3(.05f, .05f, .05f);

			//plotNode.Position = new Vector3(3 / 2f - 1, 0, 3 / 2f - 1);



			//plane.Model = ResourceCache.GetModel("Models/Mison8.mdl");
			//this.material = cache.GetMaterial("Materials/Mison8.xml");
			//this.material.SetShaderParameter("MatDiffColor", new Color(0.1f, 0.4f, 0.9f));
			//this.material.
			//plane.SetMaterial(this.material);

			//plane.Node.Translate(new Vector3(-2, 0, 1), TransformSpace.Local);



			Node lightNode = cameraNode.CreateChild(name: "light");
			var light = lightNode.CreateComponent<Light>();
			light.LightType = LightType.Point;
			light.Range = 100;
			light.Brightness = 1.3f;

			//int size = 3;
			//baseNode.Scale = new Vector3(size * 1.5f, 1, size * 1.5f);
			/*bars = new List<Bar>(size * size);
			for (var i = 0f; i < size * 1.5f; i += 1.5f)
			{
				for (var j = 0f; j < size * 1.5f; j += 1.5f)
				{
					var boxNode = plotNode.CreateChild();
					boxNode.Position = new Vector3(size / 2f - i, 0, size / 2f - j);
					var bar = new Bar(new Color(RandomHelper.NextRandom(), RandomHelper.NextRandom(), RandomHelper.NextRandom(), 0.9f));
					boxNode.AddComponent(bar);
					bar.SetValueWithAnimation((Math.Abs(i) + Math.Abs(j) + 1) / 2f);
					bars.Add(bar);
				}
			}

			SelectedBar = bars.First();
			SelectedBar.Select();*/
			//await plotNode.RunActionsAsync(new EaseBackOut(new RotateBy(2f, 0, 360, 0)));
			movementsEnabled = true;
		}

		void OnTouched(TouchEndEventArgs e)
		{
			Ray cameraRay = camera.GetScreenRay((float)e.X / Graphics.Width, (float)e.Y / Graphics.Height);
			var results = octree.RaycastSingle(cameraRay, RayQueryLevel.Triangle, 100, DrawableFlags.Geometry);
			if (results != null && results.HasValue)
			{
				var bar = results.Value.Node?.Parent?.GetComponent<Bar>();
				if (SelectedBar != bar)
				{
					SelectedBar?.Deselect();
					SelectedBar = bar;
					SelectedBar?.Select();
				}
			}
		}

		protected override void OnUpdate(float timeStep)
		{
			if (Input.NumTouches >= 1 && movementsEnabled)
			{
				var touch = Input.GetTouch(0);
				plotNode.Rotate(new Quaternion(0, -touch.Delta.X, 0), TransformSpace.Local);
			}
			base.OnUpdate(timeStep);
		}

		public void SetModel(string model, string texture)
		{
			
			plane = baseNode.CreateComponent<StaticModel>();
			plane.Model = ResourceCache.GetModel("Models/" + model);
			this.material = ResourceCache.GetMaterial("Materials/" + texture);
			//this.material.SetShaderParameter("MatDiffColor", new Color(0.1f, 0.4f, 0.9f));
			//this.material.
			plane.SetMaterial(this.material);

			//plane.Node.Translate(new Vector3(-0, 2, 1), TransformSpace.Local);
			//plane.Node.Scale = new Vector3(0.025f, 0.025f, 0.025f);

			     
		}

		public void UnsetModel()
		{
			plane.Remove();
			plane.Dispose();
			this.material.Dispose();
			/*plane.RemoveInstanceDefault();
			plane.Dispose();
			this.material.Dispose();
			Input.UnsubscribeFromAllEvents();
			scene.Dispose();*/

		}

		public void SetValue1r(float value)
		{
			this.diffColor.R = value;
			this.material.SetShaderParameter("MatDiffColor", this.diffColor);
		}

		public void SetValue1g(float value)
		{
			this.diffColor.G = value;
			this.material.SetShaderParameter("MatDiffColor", this.diffColor);
		}

		public void SetValue1b(float value)
		{
			this.diffColor.B = value;
			this.material.SetShaderParameter("MatDiffColor", this.diffColor);
		}

		public void SetValue2r(float value)
		{
			this.specColor.R = value;
			this.material.SetShaderParameter("MatSpecColor", this.specColor);
		}

		public void SetValue2g(float value)
		{
			this.specColor.G = value;
			this.material.SetShaderParameter("MatSpecColor", this.specColor);
		}

		public void SetValue2b(float value)
		{
			this.specColor.B = value;
			this.material.SetShaderParameter("MatSpecColor", this.specColor);
		}


		public void Rotate(float toValue)
		{
			plotNode.Rotate(new Quaternion(0, toValue, 0), TransformSpace.Local);
			//plotNode.Translate(new Vector3(0, toValue, 0), TransformSpace.Local);
		}
		
		void SetupViewport ()
		{
			var renderer = Renderer;
			renderer.SetViewport (0, new Viewport (Context, scene, camera, null));
		}
	}

	public class Bar : Component
	{
		Node barNode;
		Node textNode;
		Text3D text3D;
		Color color;
		float lastUpdateValue;

		public float Value
		{
			get { return barNode.Scale.Y; }
			set { barNode.Scale = new Vector3(1, value < 0.3f ? 0.3f : value, 1); }
		}

		//public void SetValueWithAnimation(float value) => barNode.RunActionsAsync(new EaseBackOut(new ScaleTo(3f, 1, value, 1)));

		public Bar(Color color)
		{
			this.color = color;
			ReceiveSceneUpdates = true;
		}

		public override void OnAttachedToNode(Node node)
		{
			barNode = node.CreateChild();
			barNode.Scale = new Vector3(1, 0, 1); //means zero height
			var box = barNode.CreateComponent<Box>();
			box.Color = color;

			textNode = node.CreateChild();
			textNode.Rotate(new Quaternion(0, 180, 0), TransformSpace.World);
			textNode.Position = new Vector3(0, 10, 0);
			text3D = textNode.CreateComponent<Text3D>();
			text3D.SetFont(Application.ResourceCache.GetFont("Fonts/Anonymous Pro.ttf"), 60);
			text3D.TextEffect = TextEffect.Stroke;
			//textNode.LookAt() //Look at camera

			base.OnAttachedToNode(node);
		}

		protected override void OnUpdate(float timeStep)
		{
			var pos = barNode.Position;
			var scale = barNode.Scale;
			barNode.Position = new Vector3(pos.X, scale.Y / 2f, pos.Z);
			textNode.Position = new Vector3(0.5f, scale.Y + 0.2f, 0);
			var newValue = (float)Math.Round(scale.Y, 1);
			if (lastUpdateValue != newValue)
				text3D.Text = newValue.ToString("F01", CultureInfo.InvariantCulture);
			lastUpdateValue = newValue;
		}

		public void Deselect()
		{
			barNode.RemoveAllActions();//TODO: remove only "selection" action
			barNode.RunActionsAsync(new EaseBackOut(new TintTo(1f, color.R, color.G, color.B)));
		}

		public void Select()
		{
			Selected?.Invoke(this);
			// "blinking" animation
			barNode.RunActionsAsync(new RepeatForever(new TintTo(0.3f, 1f, 1f, 1f), new TintTo(0.3f, color.R, color.G, color.B)));
		}

		public event Action<Bar> Selected;
	}
}