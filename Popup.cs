﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

using Xamarin.Forms;

namespace App2Heaven
{
	public class Popup : IPopupInterface
	{
		public Popup() 
		{			
		}

		public void ShowPopup()
		{
			DependencyService.Get<IPopupInterface>().ShowPopup();
		}

		public void ShowPopup(View view)
		{
			DependencyService.Get<IPopupInterface>().ShowPopup(view);
		}

		public Task<int> Show(string title, string cancel, string destruction, List<Tuple<string, int, string>> entries)
		{
			if (Device.OS == TargetPlatform.Android)
			{
				return DependencyService.Get<IPopupInterface>().Show(title, cancel, destruction, entries);
			}
			else if (Device.OS == TargetPlatform.iOS)
			{
				return DependencyService.Get<IPopupInterface>().Show(title, cancel, destruction, entries);

				/*Dictionary<string, int> contextEntries = entries.Select(x => new KeyValuePair<string, int>(x.Item1, x.Item2)).ToDictionary(x => x.Key, x => x.Value);

				TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();

				Device.BeginInvokeOnMainThread(async () => {
					string result = await App.Current.MainPage.DisplayActionSheet(null, "Abbrechen", null, contextEntries.Keys.ToArray());

					int value = 0;
					contextEntries.TryGetValue(result, out value);
					tcs.SetResult(value);
				});

				return tcs.Task;*/
			}

			return (Task<int>)null;
		}

		public Task<string> ShowPasswordDialog()
		{
			return DependencyService.Get<IPopupInterface>().ShowPasswordDialog();
		}

		public Task<string> ShowPasswordCheck()
		{
			return DependencyService.Get<IPopupInterface>().ShowPasswordCheck();
		}



	}
}

