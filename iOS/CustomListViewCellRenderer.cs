﻿using System;

using UIKit;

using Xamarin;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

using LindeGas2;

[assembly: ExportRenderer(typeof(ViewCell), typeof(LindeGas2.iOS.CustomListViewCellRenderer))]
namespace LindeGas2.iOS
{
	class CustomListViewCellRenderer : ViewCellRenderer
	{

		public override UITableViewCell GetCell(Cell item, UITableViewCell reusableCell, UITableView tv)
		{
			var cell = base.GetCell(item, reusableCell, tv);

			cell.SelectionStyle = UITableViewCellSelectionStyle.None;
			return cell;
		}
	}
}