﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;


using CoreGraphics;
using Foundation;
using ObjCRuntime;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;



namespace LindeGas2.iOS
{
	[Register("AppDelegate")]
	public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
	{
		public override bool FinishedLaunching(UIApplication app, NSDictionary options)
		{
			global::Xamarin.Forms.Forms.Init();

			LoadApplication(new App());

			var plist = NSUserDefaults.StandardUserDefaults;

			var isAcknowledged = plist.BoolForKey("IsAcknowledged");

			isAcknowledged = false;

			plist.SetBool(isAcknowledged, "IsAcknowledged");


			return base.FinishedLaunching(app, options);
		}

		public override UIInterfaceOrientationMask GetSupportedInterfaceOrientations(UIApplication application, [Transient] UIWindow forWindow)
		{
			return UIInterfaceOrientationMask.Landscape;
		}
	}
}

