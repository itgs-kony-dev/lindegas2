﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;
using System.IO;

using UIKit;

using App2Heaven;

[assembly: Xamarin.Forms.Dependency(typeof(App2Heaven.iOS.Popup))]
namespace App2Heaven.iOS
{	
	public class Popup : IPopupInterface
	{
		struct CXEntry
		{
			public string Title;
			public string Image;

		};

		public Popup()
		{			
		}

		public void ShowPopup()
		{
			/*AlertDialog.Builder builder = new AlertDialog.Builder((Context) MainActivity.Instance);
			builder.SetTitle("Test");
			string[] items = Enumerable.ToArray<string>(new string[] { "A", "B"} );
			builder.SetItems(items, (EventHandler<DialogClickEventArgs>) ((sender2, args) => {}));
			if (true)
				builder.SetPositiveButton("Cancel", (EventHandler<DialogClickEventArgs>) ((param0, param1) => {}));
			if (true)
				builder.SetNegativeButton("Destruction", (EventHandler<DialogClickEventArgs>) ((param0, param1) => {}));
			builder.SetView(new Button((Context) MainActivity.Instance) { Text = "Test" });
			AlertDialog alertDialog = builder.Create();
			builder.Dispose();
			//alertDialog.SetContentView();
			alertDialog.SetCanceledOnTouchOutside(true);
			alertDialog.CancelEvent += (EventHandler) ((sender3, e) => {});
			alertDialog.Show();*/
		}

		public Task<int> Show(string title, string cancel, string destruction, List<Tuple<string, int, string>> entries)
		{
			TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();

            try
            {
				//UIView view = new UIView();
				//view.Sh

				/*
				UIAlertView alertView = new UIAlertView();
				alertView.Title = "Request";


				nint cancelID = alertView.AddButton("Cancel");
				nint okID = alertView.AddButton("OK");

				alertView.Clicked += (object sender, UIButtonEventArgs e) =>
				{
					if (cancelID == e.ButtonIndex)
					{
						//tcs.SetResult("CANCEL");
					}
					else if (okID == e.ButtonIndex)
					{
						//tcs.SetResult(alertView.GetTextField(0).Text);
					}

				};

				alertView.Show();
				*/

				/*var rc = UIApplication.SharedApplication.KeyWindow.RootViewController;

				UIViewController vc = new UIViewController();
				vc.
				vc.Add(new UILabel { Text = "Test" });

				rc.ModalPresentationStyle = UIModalPresentationStyle.Popover;
				rc.PresentViewController(vc, true, null);
				*/

				/*UIAlertController actionSheetAlert = UIAlertController.Create(null, null, UIAlertControllerStyle.ActionSheet);

				actionSheetAlert.ModalPresentationStyle = UIModalPresentationStyle.Popover;
				//actionSheetAlert.lo
				//UIModalPresentationPopover

                for (int i = 0; i < entries.Count; i++)
                {
                    Tuple<string, int, string> entry = entries[i];

                    //var image = new UIImage(entry.Item3);
                    var alertAction = UIAlertAction.Create(title: entry.Item1, style: UIAlertActionStyle.Default, handler: (action) => tcs.SetResult(entry.Item2));
                    //alertAction.SetValueForKey(image, new Foundation.NSString("image"));

                    actionSheetAlert.AddAction(alertAction);
                }

                //actionSheetAlert.Add

                // Add Actions
                actionSheetAlert.AddAction(UIAlertAction.Create("Cancel", UIAlertActionStyle.Default, (action) => tcs.SetResult(0)));
                //actionSheetAlert(alertAction);

                var rc = UIApplication.SharedApplication.KeyWindow.RootViewController;

                rc.PresentViewController(actionSheetAlert, true, null);
				rc.ModalPresentationStyle = UIModalPresentationStyle.FormSheet;*/

				//UIAlertView alert = new UIAlertView();
				//((actionSheetAlert.

				/*UIAlertView alert = new UIAlertView();
			    alert.Title = "Title";
			    alert.AddButton("OK");
			    alert.Message = "Please Enter a Value.";
			    alert.AlertViewStyle = UIAlertViewStyle.PlainTextInput;
			    alert.Clicked += (object s, UIButtonEventArgs ev) => {
				    // handle click event here
				    // user input will be in alert.GetTextField(0).Text;
			    };

			    alert.Show();*/
			}
            catch(Exception exception)
            {
                System.Diagnostics.Debug.WriteLine("Exception:" + exception.Message);
                System.Diagnostics.Debug.WriteLine("Exception.StackTrace:" + exception.StackTrace);
            }

			    //tcs.SetResult(0);
			return tcs.Task;
		}

		public void ShowPopup(Xamarin.Forms.View view)
		{
			
		}

		public Task<string> ShowPasswordDialog()
		{
			TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();
			UIAlertController alert = UIAlertController.Create("Lock", "", UIAlertControllerStyle.Alert);


			Action<UITextField> password = (field) => {
				field.Placeholder = "Password";
				field.SecureTextEntry = true;

			};

			Action<UITextField> confirmation = (field) => {
				field.Placeholder = "Confirmation";
				field.SecureTextEntry = true;
			};

			alert.AddTextField(password);
			alert.AddTextField(confirmation);

			UITextField passwordTextField = alert.TextFields.Where(x => x.Placeholder == "Password").FirstOrDefault();
			UITextField confirmationTextField = alert.TextFields.Where(x => x.Placeholder == "Confirmation").FirstOrDefault();


			UIAlertAction click = UIAlertAction.Create(title: "Cancel", style: UIAlertActionStyle.Default, handler: (action) => tcs.SetResult("CANCEL"));
			UIAlertAction ok = UIAlertAction.Create(title: "OK", style: UIAlertActionStyle.Default, handler: (action) => {
				System.Diagnostics.Debug.WriteLine(passwordTextField.Text + " " + confirmationTextField.Text);

				if(passwordTextField.Text.CompareTo(confirmationTextField.Text) == 0)
				{
					tcs.SetResult(passwordTextField.Text);
				}
				else
				{
					UIAlertView alertView = new UIAlertView();
					//alertView.Title = "Verschlüsseln";
					alertView.Message = "Don't match";
					alertView.AddButton("OK");
					alertView.AlertViewStyle = UIAlertViewStyle.Default;

					alertView.Show();
				}
			});

			alert.AddAction(click);
			alert.AddAction(ok);

			//passwordTextField.EditingChanged += (sender, e) => System.Diagnostics.Debug.WriteLine("Changed");
			//confirmationTextField.EditingChanged += (sender, e) => System.Diagnostics.Debug.WriteLine("Changed");


			var rc = UIApplication.SharedApplication.KeyWindow.RootViewController;

			rc.PresentViewController(alert, true, null);


			return tcs.Task;
		}

		public Task<string> ShowPasswordCheck()
		{
			TaskCompletionSource<string> tcs = new TaskCompletionSource<string>();

			UIAlertView alertView = new UIAlertView();
			alertView.Title = "Request";
			//alertView.Message = "Passwortabfrage";
			nint cancelID = alertView.AddButton("Cancel");
			nint okID = alertView.AddButton("OK");
			alertView.AlertViewStyle = UIAlertViewStyle.SecureTextInput;

			alertView.Show();
			alertView.Clicked += (object sender, UIButtonEventArgs e) => {
				if(cancelID == e.ButtonIndex)
				{
					tcs.SetResult("CANCEL");
				}
				else if(okID == e.ButtonIndex)
				{				
					tcs.SetResult(alertView.GetTextField(0).Text);
				}

			};

			return tcs.Task;

		}

	}
}

