﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Diagnostics;

using UIKit;
using Contacts;
using Foundation;

using Xamarin;
using Xamarin.Forms;

using LindeGas2;

[assembly: Xamarin.Forms.Dependency(typeof(LindeGas2.iOS.UserSettings))]
namespace LindeGas2.iOS
{
	
	public class UserSettings : IUserSettings
	{
		public UserSettings()
		{
			
		}

		public void Save(string key, object value)
		{
			var plist = NSUserDefaults.StandardUserDefaults;

			if(value is bool)
				plist.SetBool((bool) value, key);
		}

		public bool RestoreBool(string key)
		{
			var plist = NSUserDefaults.StandardUserDefaults;

			return plist.BoolForKey(key);
		}
	}
}
