﻿using System;

using Xamarin.Forms;

namespace LindeGas2
{
	public class Model3DScan : ModelBase, ICombination
	{
		public string Metal { get; set; }
		public string Process { get; set; }
		public string Gas { get; set; }
		public string Filename { get; set; }
		public string Material { get; set; }
		public string Preview { get; set; }


	}
}

