﻿using System;
namespace LindeGas2
{
	public interface ICombination
	{
		string Metal { get; set; }
		string Process { get; set; }
		string Gas { get; set; }
	}
}

