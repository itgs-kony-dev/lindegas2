﻿using System;

namespace LindeGas2
{
	public class Gas : ModelBase
	{
		public int ID;
		public string Name { get; set; }
		public UnselectedCell Cell { get; set; }
		public bool IsPremium;

		public bool isSelected;

		public override string ToString()
		{
			return Name;
		}

		public bool IsSelected
		{
			set
			{
				this.isSelected = value;
				OnPropertyChanged("IsSelected");
			}
			get { return this.isSelected; }
		}
	}
}

