﻿using System;

using Xamarin.Forms;

namespace LindeGas2
{
	public class TechnicalData : ICombination
	{
		public string Metal { get; set; }
		public string Process { get; set; }
		public string Gas { get; set; }
		public int WeldingSpeed { get; set; }
		public int PorosityControl { get; set; }
		public int Fusion { get; set; }
		public int Penetration { get; set; }
		public int EaseOfUse { get; set; }
		public double ThicknessRangeFrom { get; set; }
		public double ThicknessRangeTo { get; set; }
		public string Description { get; set; }

	}
}

