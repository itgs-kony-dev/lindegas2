﻿using System;

namespace LindeGas2
{
	public class Metal
	{
		public string Name { get; set; }

		public override string ToString()
		{
			return Name.ToString();
		}
	}
}

