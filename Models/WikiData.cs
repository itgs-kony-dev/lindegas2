﻿using System;

using Xamarin.Forms;

namespace LindeGas2
{
	public class WikiData : ModelBase, ICombination
	{
		public string Metal { get; set; }
		public string Process { get; set; }
		public string Gas { get; set; }
		public string filename { get; set; }
		public string filename2 { get; set; }

		public string FileName
		{
			get { return filename; }
		}

		public string FileName2
		{
			set
			{
				this.filename2 = value;
				OnPropertyChanged("FileName2");
			}
			get { return this.filename2; }
		}
	}
}

