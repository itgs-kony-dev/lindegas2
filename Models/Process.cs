﻿using System;

namespace LindeGas2
{
	public class Process
	{
		public string Name { get; set; }
		public bool isPremium { get; set; }

		public override string ToString()
		{
			return Name.ToString();
		}
	}
}

