﻿using System;

using Xamarin.Forms;

namespace LindeGas2
{
	public class CrossSection : ModelBase, ICombination
	{
		public string Metal { get; set; }
		public string Process { get; set; }
		public string Gas { get; set; }
		public string Filename { get; set; }

		public string Type { get; set; }

		private string filename2;

		public ImageSource ImageSource
		{
			get { return ImageSource.FromResource(Filename); }
		}

		public ImageSource ImageSource2
		{
			get { return ImageSource.FromResource(Filename2); }
		}

		public string Filename2
		{
			set
			{
				this.filename2 = value;
				OnPropertyChanged("ImageSource2");
			}
			get { return this.filename2; }
		}
	}
}

