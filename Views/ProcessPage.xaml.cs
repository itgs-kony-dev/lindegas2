﻿using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;

namespace LindeGas2
{
	public partial class ProcessPage : ContentPage
	{
		public Metal Material { get; set; }

		IEnumerable<Process> processes;

		public ProcessPage()
		{
			InitializeComponent();

			Title = "Process";

			this.processes = App.RootObject.Processes;
		}

		public void OnGMAWSelected(object sender, EventArgs args)
		{
			Navigation.PushAsync(new LindeGas2Page() { Material = Material, Process = this.processes.FirstOrDefault(x => x.Name == "GMAW") });
		}

		public void OnGTAWSelected(object sender, EventArgs args)
		{
			Navigation.PushAsync(new LindeGas2Page() { Material = Material, Process = this.processes.FirstOrDefault(x => x.Name == "GTAW") });
		}

	}
}
