﻿using System;

namespace LindeGas2
{
	public interface IUserSettings
	{
		void Save(string key, object value);
		bool RestoreBool(string key);
	}
}
